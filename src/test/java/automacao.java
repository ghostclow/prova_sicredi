import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class automacao {
    @Test
    public void acessartest() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "src\\drive\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        // ACESSAR O CAMPO DE FORUMULARIO //

        driver.get("https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");
        driver.findElement(By.xpath("//select[@id='switch-version-select']")).click();
        driver.findElement(By.xpath("//option[@value='/v1.x/demo/my_boss_is_in_a_hurry/bootstrap-v4']")).click();
        driver.findElement(By.xpath("//div[@class='floatL t5']//a[@class='btn btn-default btn-outline-dark']")).click();

        // NOME DO CADASTRO //

        WebElement name = driver.findElement(By.id("field-customerName"));
        name.sendKeys("Teste Sicredi");
        WebElement last = driver.findElement(By.id("field-contactLastName"));
        last.sendKeys(("Teste"));
        WebElement first = driver.findElement(By.id("field-contactFirstName"));
        first.sendKeys("Juan Pablo");

        // TELEFONE DO CADASTRO

        WebElement phone = driver.findElement(By.id("field-phone"));
        phone.sendKeys("51 9999-9999");

        // ENDEREÇO DO CADASTRO//

        WebElement adress1 = driver.findElement(By.id("field-addressLine1"));
        adress1.sendKeys("Av Assis Brasil, 3970");
        WebElement adress2 = driver.findElement(By.id("field-addressLine2"));
        adress2.sendKeys("Torre D");
        WebElement city = driver.findElement(By.id("field-city"));
        city.sendKeys("Porto Alegre");
        WebElement state = driver.findElement(By.id("field-state"));
        state.sendKeys("RS");
        WebElement postal = driver.findElement(By.id("field-postalCode"));
        postal.sendKeys("91000-000\n");
        WebElement country = driver.findElement(By.id("field-country"));
        country.sendKeys("Brasil");

        // INFORMACOES ADICIONAIS //

        WebElement sales = driver.findElement(By.id("field-salesRepEmployeeNumber"));
        sales.sendKeys("0");
        WebElement credit = driver.findElement(By.id("field-creditLimit"));
        credit.sendKeys("200");

        // BOTAO SALVAR //

        WebElement save = driver.findElement(By.id("form-button-save"));
        save.click();
        Thread.sleep(3000);
        driver.switchTo().parentFrame();
        System.out.println(driver.getTitle());

        // BOTAO PARA VOLTAR PAGINA INICIAL

        WebElement back = driver.findElement(By.id("save-and-go-back-button"));
        back.click();
        Thread.sleep(4000);

        // PROCURAR NOME //

        WebElement search = driver.findElement(By.name("customerName"));
        search.sendKeys("Teste Sicredi");
        Thread.sleep(4000);

        //MARCAR CHECKBOX //

        driver.findElement(By.className("select-all-none")).click();
        Thread.sleep(3000);
        driver.findElement(By.className("select-all-none")).click();
        Thread.sleep(3000);
        driver.switchTo().parentFrame();
        System.out.println(driver.getTitle());

        // DELETAR //

        driver.findElement(By.linkText("Delete")).click();
        Thread.sleep(3000);
        driver.switchTo().parentFrame();
        System.out.println(driver.getTitle());
        driver.findElement(By.cssSelector(".btn.btn-danger.delete-multiple-confirmation-button")).click();
        driver.switchTo().parentFrame();
        System.out.println(driver.getTitle());

        Thread.sleep(10000);


        // FECHAR //

        driver.close();




    }
}
